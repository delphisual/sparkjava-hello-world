package com.example;

import static spark.Spark.get;

import spark.servlet.SparkApplication;

public class HelloWorld implements SparkApplication {
	public static void main(String[] args) {
		get("/hello", (req, res) -> "Hello World");
	}

	@Override
	public void init() {
		get("/hello", (req, res) -> "Hello World");
	}
}